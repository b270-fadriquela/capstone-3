
import { Navigate } from "react-router-dom";
import { useEffect, useContext } from 'react';

import UserContext from '../context/UserContext';


export default function Logout(){


	const { unsetUser, setUser} = useContext(UserContext);
	//window.dispatchEvent(new Event("logoutEvent"));
	unsetUser();

	useEffect(() => {
			setUser({id:null})
			
	});

	return(

		<Navigate to='/' />
	)
}