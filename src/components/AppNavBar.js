import Container from 'react-bootstrap/Container';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { Fragment, useState, useEffect, useContext} from 'react';
import { Link, NavLink} from 'react-router-dom';

import UserContext from '../context/UserContext';


export default function AppNavBar()
{
	const { user } = useContext(UserContext)

		console.log(user.userName);
	return(
			<Navbar bg="light" expand="lg">
		      <Container fluid>
		        <Navbar.Brand as={Link} to="/">Fume</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		           {(user.id === null)
		          	?
		          		<Fragment>
			            <Nav.Link as={Link} to="/Login">Login</Nav.Link>
			            <Nav.Link as={Link} to="/Register">Register</Nav.Link>
			            </Fragment>
		       		:
			       		<Fragment>
		        		<Navbar.Collapse id="basic-navbar-nav">
			            		<Nav.Link as={Link} to="/Logout">Logout</Nav.Link>
		        		</Navbar.Collapse>
			       		</Fragment>
		       		}
		          </Nav>
		        </Navbar.Collapse>

		        <div className="me-5">
		        </div>

		        <div>
		        {(user.id !== null)

		        	?
		        <Navbar.Brand as={Link} to="/profile">Welcome {user.userName}</Navbar.Brand>
		        	:
		        	<Fragment>
		        	</Fragment>
		        }
		        </div>

		      </Container>
		    </Navbar>
	);
		
}

