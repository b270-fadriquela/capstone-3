import {Row, Col, Card, Button, Form, Container, Dropdown} from 'react-bootstrap';

import { useState, useEffect, React, Fragment, useContext} from 'react';
import ReactSwitch from 'react-switch';
import Swal from 'sweetalert2';

import { Link } from 'react-router-dom';
import UserContext from '../context/UserContext';

export default function OrderItem(data, index, isCart) {

	const {_id, productName, quantity, purchaseDate, isTrial, productId, total} = data.data;
	const {number} = data.index;
	const rowStyle = () => {

		if(number % 0){
			return 'bg-primary m-2 py-1';
		}
			return 'bg-success m-2  py-1'


	} 

	const removeToCart = (productId) => {
		
		  fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/remove`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		       	body: JSON.stringify({
		       		quantity: quantity,
		       		isTrial: false
		       	})
	      	 })
		    .then(res => res.json())
		    .then(data => {
		    	console.log("remove " + data);
		    	changeStocks(productId)

		    	   Swal.fire({
					title: "Success",
					icon: "info",
					text: "Item Remove from cart!"
				})

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}

	const changeStocks = (productId) => {
		
		  fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/stocks`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		       	body: JSON.stringify({
		       		quantity: quantity,
		       	})
	      	 })
		    .then(res => res.json())
		    .then(data => {
		    	//refresh

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	} 

	return(
		<Container>

		<Row className={rowStyle()}>

			<Col>
				prod:{productName}
			</Col>

			<Col>
				isTrial:false
			</Col>

			<Col>
				quantity:{quantity}
			</Col>
			{(data.isCart === true)
				?
			<Col>
				subtotal:{total}
			</Col>
			:
			<Col />
			}
			
			{(data.isCart === true)
				?
			<Col>
		    	<Button variant="danger" onClick={() => removeToCart(productId)}>Remove To Cart</Button>

			</Col>
			:
			<Col>
		    
			</Col>
			}


		</Row>

		</Container>
		
	);
}