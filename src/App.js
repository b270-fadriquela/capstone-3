
import './App.css';


//COMPONENTS
import AppNavBar from './components/AppNavBar';
import AdminNavBar from './components/AdminNavBar';
import CategoryNavBar from './components/Category';

//PAGES
import PageNotFound from './pages/PageNotFound';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Home from './pages/Home';
import Profile from './pages/Profile';

import Products from './pages/admin/Products';
import Users from './pages/admin/User';
import UserOrders from './pages/admin/UserOrders';
import ViewProduct from './pages/admin/ViewProduct';
import CreateProduct from './pages/admin/CreateProduct';

import ItemView from './pages/ItemView';
import History from './pages/OrderHistory';
import Cart from './pages/ViewCart';
// import User from './pages/User';
// import ViewUser from './pages/ViewUser';


import { Container } from 'react-bootstrap'
import { BrowserRouter as Router} from 'react-router-dom'
import { Route, Routes }  from 'react-router-dom'
import { Fragment, useState, useEffect  } from 'react'
import { UserProvider } from './context/UserContext';


function App() {


  const [user, setUser] = useState({
    id:"",
    userName:"",    
    isAdmin: undefined
  });

  useEffect(()=>{
      

        fetch(`${process.env.REACT_APP_API_URL}/profile`, {
          headers: {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
         })
      .then(res => res.json())
      .then(data => {
        console.log("==============");
        console.log(data)
        setUser({
          id: data._id,
          userName: data.userName,
          isAdmin: data.isAdmin
        });
      })
      .catch(rejected => {
          console.log(rejected);
      });
   }, [])



  const unsetUser = () => {
    localStorage.clear();
  }
  return (

      <UserProvider value={{user, unsetUser , setUser}}>
        <Router>
          <Container fluid>

            {(user.isAdmin === true)
            ?
            <Container>
              <AdminNavBar />
            </Container>
            :
            <Container>
              <AppNavBar />
              <CategoryNavBar />
            </Container>
           

            }
            <Routes>
              <Route path="*" element={<PageNotFound />} />
              <Route path="/" element={<Home />} />
              <Route path="/:category" element={<Home/>} />
              <Route path="/Login" element={<Login />} />
              <Route path="/Register" element={<Register />} />
              <Route path="/Logout" element={<Logout />} />
              <Route path="/Profile" element={<Profile />} />

                <Route path="/cart" element={<Cart />} />
                <Route path="/history" element={<History />} />


              {(user.isAdmin === true)
              ?
              <Fragment>
               
                <Route path="/Products" element={<Products />} />
                <Route path="/Products/:productId" element={<ViewProduct />} />
                <Route path="/Products/create" element={<CreateProduct />} />
                <Route path="/Users" element={<Users />} />
                <Route path="/Users/Orders" element={<UserOrders />} />

              </Fragment>
              :

              <Fragment>
                <Route path="/Products/:productId" element={<ItemView />} />
              </Fragment>
              }

            </Routes>


          </Container>
        </Router>
      </UserProvider>
  );
}


export default App;
