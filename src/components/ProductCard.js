import {Row, Col, Card, Button,ListGroup } from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment } from 'react';
import { Link, Navigate } from 'react-router-dom';
import { Buffer } from 'buffer';


//View
export default function ProductCard({data}) {
	const {_id, name, description, price, stocks, createdOn, preview} = data;
	let imgData;


	const width = window.innerWidth;

		if(preview !== undefined){
			let data = JSON.parse(Buffer.from(preview,'base64').toString());
			imgData = data['newFilename'] +"."+ data['mimetype'].slice(6,data['mimetype'].length);
			console.log(imgData);
		}

	return(

		<div className="m-1">
		<Card >
		     <Card.Img variant="top" src="placeholder" />
		     <Card.Body>
		       <Card.Title>{name}</Card.Title>
		       <Card.Text>{description}</Card.Text>
		     </Card.Body>
		     <ListGroup className="list-group-flush">
		       <ListGroup.Item>Php. {price}</ListGroup.Item>
		     </ListGroup>

		      <Button  variant="primary" as={Link} to={`/products/${_id}`}>View</Button>
		   </Card>
		</div>
		
	)

}

