import Container from 'react-bootstrap/Container';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { Fragment, useState, useEffect, useContext} from 'react';
import { Link, NavLink} from 'react-router-dom';

import UserContext from '../context/UserContext';

import {Row, Col, Card, Button} from 'react-bootstrap';

export default function AdminNavBar()
{
	const { user } = useContext(UserContext)

	return(
			<Navbar bg="light" expand="lg">
		      <Container fluid>
		        <Navbar.Brand as={Link} to="/">The Backrooms</Navbar.Brand>

		        <Navbar.Toggle aria-controls="basic-navbar-nav" />

		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		          <Nav.Link as={Link} to="/Products">View Products</Nav.Link>
		          <Nav.Link as={Link} to="/Users">View Users</Nav.Link>
		          <Nav.Link as={Link} to="/Users/Orders">View Users Orders</Nav.Link>
		          <Nav.Link as={Link} to="/Logout">Logout</Nav.Link>
		          </Nav>

		        </Navbar.Collapse>
		  		
		      </Container>
		    </Navbar>
	);
		
}

