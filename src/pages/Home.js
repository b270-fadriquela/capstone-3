import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment } from 'react';
import { Link, useParams} from 'react-router-dom';


import ProductCard from '../components/ProductCard';
import Banners from '../components/Banner';
import Searchbar from '../components/Searchbar';
//View
export default function Home() {

	const [products, setProducts] = useState([]);
	const { category } = useParams();

	const style = "d-flex align-items-center justify-content-center mt-5 p-5 w-100";



	useEffect(()=>{
	let request ;
	
	if(category === undefined)
		request = `${process.env.REACT_APP_API_URL}/products`;
	else
		request = `${process.env.REACT_APP_API_URL}/${category}`
	
	  console.log(request);  	    	
	     fetch(request, {
                   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
	         })
		    .then(res => res.json())
		    .then(data => {
		    	const style = "d-flex align-items-center justify-content-center ";
		    	console.log(data);
		    	setProducts (data.map(product =>{
		    		return(
		    			<Col className={style}>
			    			<ProductCard key={product._id} data={product} />
			    		</Col>
		    			
		    		)
		    	}))	
		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [category]);



	return(
		<Container>
			 <Banners />
			 <Row className={style}>
			 	{products}
			 </Row>
		</Container>
		
	)

}