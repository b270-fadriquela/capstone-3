import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import { Navigate } from "react-router-dom";


import UserContext from '../context/UserContext';


export default function Register() {


	const { user, setUser } = useContext(UserContext)
	const [ email, setEmail ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");

	const [ userName, setUserName ] = useState("");

	const [ isActive, setIsActive ] = useState(false);


	const getUserDetails = (token) => { 

	        fetch(`${process.env.REACT_APP_API_URL}/profile`, {
	           headers: { 'Authorization': `Bearer ${token}` }
	        })
	        .then(res => res.json())
	        .then(data => {
	        	console.log(data);
	        	if(data !==	undefined){
		        	setUser({
	                    id:data._id,
	                    isAdmin: data.isAdmin,
	                    userName: data.userName
	                });
	        	}

	        })
	        .catch(rejected => {
	            console.log(rejected);
	        });
	};



	const verifyEmail = (e) => {
		e.preventDefault();

		//TODO: Check Email in BackEnd

		fetch(`${process.env.REACT_APP_API_URL}/signup`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				userName: userName,
				email: email,
				password: password1    
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data!==undefined) {
				//Access Token Recieved here too!
				//Get User Details
				console.log(data.accessToken);
				localStorage.setItem('token', data.accessToken);
				getUserDetails(data.accessToken);

				Swal.fire({
					title: "Registration Complete",
					icon: "info",
					text: "Registration Successful"
				})
				

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})


	}

	useEffect(() => {
		if(userName !== "" && password1 === password2 && (password1 !== "" && password2 !== "" && email !== ""))
			setIsActive(true);
		else
			setIsActive(false)

	}, [password2 , password1, email, userName])

	//TODO expand registration
	return(

	(user.id !== null)
    ?
    	  <Navigate to="/"/ >
    :
		<Container>
		<Form className="p-2"onSubmit={(e) => verifyEmail(e)}>
		<h1>Register</h1>

			<Form.Group className="mb-3" controlId="userProfileName">
			  <Form.Label>User Name</Form.Label>
			  <Form.Control 
			  		type="text" 
			  		placeholder="User Name" 
			  		required
			  		onChange={e => setUserName(e.target.value)}
			  		value={userName}/>
			
			</Form.Group>

		     <Form.Group className="mb-3" controlId="userEmail">
		       <Form.Label>Email address</Form.Label>
		       <Form.Control 
		       		type="email" 
		       		placeholder="Email" 
		       		required
		       		onChange={e => setEmail(e.target.value)}
		       		value={email}/>
		       <Form.Text className="text-muted">
		         We'll never share your email with anyone else.
		       </Form.Text>
		     </Form.Group>
		     
		     <Form.Group className="mb-3" controlId="password1">
		       <Form.Label>Password</Form.Label>
		       <Form.Control 
		       		type="password" 
		       		placeholder="Password" 
		       		required
		       		onChange={e => setPassword1(e.target.value)}
		       		value={password1}/>
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="password2">
		       <Form.Label>Verify Password</Form.Label>
		       <Form.Control 
		       		type="password" 
		       		placeholder="Password" 
		       		required
		       		onChange={e => setPassword2(e.target.value)}
		       		value={password2}/>
		     </Form.Group>


		    {isActive
		     ?
		     <Button variant="primary" type="submit">Submit</Button>
		     :
		     <Button variant="danger" type="submit" disabled>Submit</Button>
			}
		</Form>
		</Container>
	);
}