import {Row, Col, Card, Button, Form, Container, Dropdown} from 'react-bootstrap';

import { useState, useEffect, React, Fragment} from 'react';
import ReactSwitch from 'react-switch';

import { Link, useParams } from 'react-router-dom';

export default function ViewUsers() {

	const { productId } = useParams();
	const [changes, setChanges] = useState(false)

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [stocks, setStocks] = useState("")
	const [createdOn, setCreatedOn] = useState("")
	const [category, setCategory] = useState("")
	const [isActive, setActive] = useState(false)

	useEffect(() => {

		
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`, {
		   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
		})
		.then(res => res.json())
		.then(data => {
			if(data!==undefined){
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setStocks(data.stocks);
				setCreatedOn(data.createdOn);
				setActive(data.isActive);
				setCategory(data.category);
			}
		})

	}, [productId])

	const updateProduct = (e) => { 
		e.preventDefault();


		console.log(`Bearer ${localStorage.getItem('token')}`)

	      	 fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		       	body: JSON.stringify({
		       		name: name,
		       		description: description,
		       		price: price,
		       		stocks: stocks,
		       		createdOn: createdOn,
		       		isActive: isActive,
		       		category: category
		       	})
	      	 })


	        .then(res => res.json())
	        .then(data => {
	            console.log(data);
	            console.log("TODO add alert")


	        })
	        .catch(rejected => {
	            console.log(rejected);
	        });
    }; 

    const handleSwitchChange = val => {

    	setActive(val)

    }

	return(
		<Fragment>
			<Card className="cardCourses mt-2">
			<Container fluid>
					<h1> Edit Product </h1>
					<Button variant="danger" as={Link} to={`/Products/`}>Return</Button>
			</Container>
			<Form className="p-2" onSubmit={(e) => updateProduct(e)}> 
					<h3>id:{productId}</h3>


					     <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Name</Form.Label>
					       <Form.Control 
					       		type="text" 
					       		placeholder="loading.." 
					       		required
					       		onChange={e => setName(e.target.value)}
					       		value={name}/>
					     </Form.Group>

					     <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Description</Form.Label>
					       <Form.Control 
					       		type="text" 
					       		placeholder="loading.." 
					       		required
					       		onChange={e => setDescription(e.target.value)}
					       		value={description}/>
					     </Form.Group>

					     <Row>
					       <Form.Label>Category</Form.Label>
					     <Col>
					      <Form.Group className="mb-3" controlId="category">
					       <Form.Control 
					       		disabled
					       		type="text" 
					       		placeholder="Select Category" 
					       		required
					       		onChange={e => setPrice(e.target.value)}
					       		value={category}/>
					     </Form.Group>
					     </Col>

					     <Col>
					     <Dropdown onSelect={function(e){setCategory(e)}}>
					          <Dropdown.Toggle variant="dark" id="dropdown-basic">
					          </Dropdown.Toggle>

					          <Dropdown.Menu>
					            <Dropdown.Item eventKey="steam">Steam</Dropdown.Item>
					            <Dropdown.Item eventKey="origin">Origin</Dropdown.Item>
					            <Dropdown.Item eventKey="ps5">PS5</Dropdown.Item>
					            <Dropdown.Item eventKey="ps4">PS4</Dropdown.Item>
					          </Dropdown.Menu>
				        </Dropdown>
					        </Col>
					        </Row>

					      <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Price</Form.Label>
					       <Form.Control 
					       		type="text" 
					       		placeholder="loading.." 
					       		required
					       		onChange={e => setPrice(e.target.value)}
					       		value={price}/>
					     </Form.Group>

					      <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Stocks</Form.Label>
					       <Form.Control 
					       		type="text" 
					       		placeholder="loading.." 
					       		required
					       		onChange={e => setStocks(e.target.value)}
					       		value={stocks}/>
					     </Form.Group>

					       <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Modification Date</Form.Label>
					       <Form.Control 
					       		disabled
					       		type="text" 
					       		placeholder="loading.." 
					       		required
					       		value={createdOn}/>
					     </Form.Group>

					    <Row className="pb-4">
					    <Form.Label>Active Product</Form.Label>
						    <Row>
						    <ReactSwitch
						            checked={isActive}
						            onChange={handleSwitchChange}
						          />
						    </Row>
					    </Row>
					  

					    <Button variant="primary" type="submit">Update</Button>
					</Form>
			</Card>

			<Card className="cardCourses mt-2">
			</Card>

		</Fragment>
		
	);
}