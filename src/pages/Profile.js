import { useState, useEffect, useContext, Fragment} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../context/UserContext';


export default function Profile() {

	const { user } = useContext(UserContext);

	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [newPassword, setNewPassword] = useState("");


	const changePassword = (userId) => {

	    		console.log("data" + userId);
		  fetch(`${process.env.REACT_APP_API_URL}/profile/changePassword`, {
	       	method: "PATCH",
	       	headers: {
	       		"Content-Type" : "application/json",
	       		Authorization: `Bearer ${localStorage.getItem('token')}`
	       	},
	       	body: JSON.stringify({
	       		userId: userId,
	       		newPassword: newPassword
	       	})
      	 })
	    .then(res => res.json())
	    .then(data => {
	    		console.log(data);
	    		Swal.fire({
		    		icon: "info",
		    		text: "Password Successfully Changed!"
		    	})
	    })
	    .catch(rejected => {
	        console.log(rejected);
	    });

	}


	useEffect(() => {
		  fetch(`${process.env.REACT_APP_API_URL}/profile`, {
	       	headers: {
	       		"Content-Type" : "application/json",
	       		Authorization: `Bearer ${localStorage.getItem('token')}`
	       	}
      	 })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data)
	    	setName(data.userName);
	    	setEmail(data.email);

	    })
	    .catch(rejected => {
	        console.log(rejected);
	    });

	}, [])
	

	return(
		<Container>
			<Row>
				<Col>
					<Card>
					    <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Text></Card.Text>
					        <Card.Subtitle>Email: {email}</Card.Subtitle>
					        <Container className="mt-5">
					        <Card.Subtitle>New Password</Card.Subtitle>
					        <Form.Control 
					        		type="password" 
					        		placeholder="Password" 
					        		required
					        		onChange={e => setNewPassword(e.target.value)}
					        		/>

					        <Button className = "mt-2" variant="primary" onClick={() => changePassword(user.id)}>Change Password</Button>
					        </Container>
					       
					    </Card.Body>
					</Card>
				</Col>
						<Button  variant="warning" as={Link} to={`/`}>Back</Button>
			</Row>
		</Container>
	)
}