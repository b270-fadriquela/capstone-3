import {Row, Col, Card, Button, Form, Container, Dropdown} from 'react-bootstrap';

import { useState, useEffect, React, Fragment, useContext} from 'react';
import ReactSwitch from 'react-switch';

import { Link } from 'react-router-dom';
import OrderItem from '../../components/OrderItem';

export default function UserOrderHistory() {


	const [order, setOrder] = useState([]);

	useEffect(()=>{
	     fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
                   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
	         })
		    .then(res => res.json())
		    .then(data => {
		    	
		    	let compiledOrders= [];

		    	data.forEach((user) => {
		    		compiledOrders = jsonConcat(compiledOrders, user.orders);
		    		
		    	})
		    	console.log(compiledOrders)

		    	setOrder (compiledOrders.map(product =>{
			    		return(
			    			<OrderItem key={product.productId} data={product} index = {1} isCart={false}/>
			    		)
			    	}))	

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [])
// isTrial
// : 
// false
// productName
// : 
// "Spellforce 3 Reforged"
// purchaseDate
// : 
// "2023-06-14T03:11:56.140Z"
// quantity
// : 
// 5
// _id
// : 
// "6489292e8a398e4636c581cc"


	function jsonConcat(target, source){
		for(var key in source)
			target.push({
				"productName": source[key].productName,
				"isTrial": source[key].isTrial,
				"purchaseDate": source[key].purchaseDate,
				"quantity": source[key].quantity,
				"_id": source[key]._id,
			});

		return target;
	}

	return(
		<Container>
				<Row>
					<Col>
					all user orders
					{order}
					</Col>
				</Row>

		</Container>
		
	);
}