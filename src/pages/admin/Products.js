import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment} from 'react';
import { Link } from 'react-router-dom';


import ProductItem from '../../components/ProductItem';
//View
export default function Products() {

	const [products, setProducts] = useState([]);

	const productHeader = 
	{
		_id: "id", 
		name:  "Name", 
		description: "Description", 
		price:  "Price", 
		stocks:  "Stocks",
		createdOn: "Last Update",
	};
	useEffect(()=>{

	     fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
                   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
	         })
		    .then(res => res.json())
		    .then(data => {

		    	setProducts (data.map(product =>{
		    		return(
		    			<ProductItem key={product._id} data={product} />
		    		)
		    	}))	
		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [])


	return(
		<Container>
			 <h1>Available Products</h1>
			 <ProductItem data={productHeader} />
			 {products}

			 <Col className="mx-2">
			 
			 
				<Button  variant="primary" as={Link} to={`/Products/create`}>Add New</Button>
			</Col>

		</Container>
		
	)

}