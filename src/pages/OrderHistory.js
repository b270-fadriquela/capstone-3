import {Row, Col, Card, Button, Form, Container, Dropdown} from 'react-bootstrap';

import { useState, useEffect, React, Fragment, useContext} from 'react';
import ReactSwitch from 'react-switch';

import { Link } from 'react-router-dom';
import UserContext from '../context/UserContext';
import OrderItem from '../components/OrderItem';

export default function OrderHistory() {

	const { user } = useContext(UserContext)

	const [order, setOrder] = useState([]);

	useEffect(()=>{

	
	     fetch(`${process.env.REACT_APP_API_URL}/profile`, {
                   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
	         })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data.cart);
		    	setOrder (data.orders.map(product =>{
		    		return(
		    			<OrderItem key={product._id} data={product} index = {0} isCart={false} />
		    		)
		    	}))	
		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [])

	return(
		<Container>
				<Row>
					<Col>
					history
					{order}
					</Col>
				</Row>

		</Container>
		
	);
}