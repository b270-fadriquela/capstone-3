import { useState, useEffect, useContext, Fragment} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../context/UserContext';


export default function ItemView() {

	const { user } = useContext(UserContext);
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [category, setCategory] = useState("");
	const [maxStocks, setMaxStocks]  = useState(0);
	const [hasCart, setHasCart]  = useState(false);
	const [purchaseAmt, setPurchaseAmt]  = useState(0);


	const addToCart = (productId) => {

		if({stocks} === 0)
		{
			alert("invalid pruchase")
		}
		else
			console.log("attempt add cart")
			  fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/add`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		       	body: JSON.stringify({
		       		quantity: maxStocks - stocks,
		       		isTrial: false
		       	})
	      	 })
		    .then(res => res.json())
		    .then(data => {
		    	Swal.fire({
		    		icon: "info",
		    		text: "Item Added to Cart"
		    	})
		    	changeStocks(productId);

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}

	const changeStocks = (productId) => {
		
		  fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/stocks`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		       	body: JSON.stringify({
		       		quantity: -(maxStocks - stocks),
		       	})
	      	 })
		    .then(res => res.json())
		    .then(data => {
		    	//refresh

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	} 


	const incPurchaseCount = () => {
		if(stocks-1 >= 0)
		{
			setStocks(stocks-1);
			setPurchaseAmt(purchaseAmt+1);
		}
	}

	const decPurchaseCount = () => {
		if(maxStocks >= stocks+1)
		{
			setStocks(stocks+1);
			setPurchaseAmt(purchaseAmt-1);
		}window.location.reload();
	}


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
			setMaxStocks(data.stocks);
			setCategory(data.category);
		})

	}, [productId])

	return(
		<Container>
			<Row>
				<Col>
					<Card>
					    <Card.Body className="text-center">
					    	<Card.Img variant="top" src="placeholder" />
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>{category}</Card.Subtitle>
					        <Card.Subtitle>{description}</Card.Subtitle>
					        <Card.Text></Card.Text>
					        <Card.Subtitle>Price: {price}</Card.Subtitle>

					        <Form.Label className = "mt-5"  >Stocks Remaining: {stocks}</Form.Label>
					        <Container>
					        <Row>
						        <Col>
						        <Button  variant="danger" onClick={() => decPurchaseCount()}>-</Button>
						        </Col>
						        <Col>
						         <Form.Control className="text-center"
						       		disabled
						       		type="text" 
						       		placeholder="0" 
						       		value= {purchaseAmt} />
						       		</Col>
						       	<Col>
						        <Button  variant="success" onClick={() => incPurchaseCount()}>+</Button>
						        </Col>
						        </Row>
					       	</Container>

					        {user.id !== null
					        	?
					        	<Fragment>
					        	<Button className = "m-5" variant="primary" onClick={() => addToCart(productId)}>Add To Cart</Button>
					        	</Fragment>
					        	:
					        	<Fragment></Fragment>
					        }
					    </Card.Body>
					</Card>
				</Col>
						<Button  variant="warning" as={Link} to={`/`}>Back</Button>
			</Row>
		</Container>
	)
}