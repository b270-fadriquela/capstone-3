import {Row, Col, Card, Button} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { CCarousel, CCarouselItem, CImage } from '@coreui/react';

import image1 from  '../images/feature1.gif' 
import image2 from  '../images/feature2.gif' 
import image3 from  '../images/feature3.gif' 
//View
export default function Banner() {


	const style = "d-block w-100"


	return(
			<CCarousel controls>
			  <CCarouselItem>
			    <CImage className={style} src={image1} alt="slide 1" />
			  </CCarouselItem>
			  <CCarouselItem>
			    <CImage className={style} src={image2} alt="slide 2" />
			  </CCarouselItem>
			  <CCarouselItem>
			    <CImage className={style} src={image3} alt="slide 3" />
			  </CCarouselItem>
			</CCarousel>
	)

}

