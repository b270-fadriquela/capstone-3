import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext, } from 'react';
import { Navigate, useLocation} from "react-router-dom";
import  Swal  from 'sweetalert2';

import UserContext from '../context/UserContext';


export default function Login() {


	const { user, setUser } = useContext(UserContext)

	const [ userName, setUserName ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ isActive, setIsActive ] = useState(true);


	const getUserDetails = (token) => { 


	        fetch(`${process.env.REACT_APP_API_URL}/profile`, {
	           headers: { 'Authorization': `Bearer ${token}` }
	        })
	        .then(res => res.json())
	        .then(data => {
	        	console.log(data);
	        	if(data !==	undefined){
		        	setUser({
	                    id:data._id,
	                    isAdmin: data.isAdmin,
	                    userName: data.userName
	                });
	                
	                Swal.fire({
		    		icon: "info",
		    		text: "Login Succesful!"
			    	})

	        	}

	        })
	        .catch(rejected => {
	            console.log(rejected);
	        });
	};

	const attemptLogin = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/login`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				userName: userName,
				password: password
			})
		})
		.then(res => res.json())
		        .then(data => {

					console.log(data);
		            if(data.accessToken !== undefined){
		                localStorage.setItem('token', data.accessToken);

		                getUserDetails(data.accessToken)

		            }
		            else{
		             
		            }
		            
        })
        .catch(rejected => {
            console.log(rejected);
        });
	}

	return (

	(user.id !== null)
    ?
    	  <Navigate to="/"/ >
    :
    <Container>
		<Form onSubmit={(e) => attemptLogin(e)}>
		<h1>Login</h1>

		    <Form.Group controlId="userEmail">
		        <Form.Label>User Name</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Enter user name"
		            value={userName}
		            onChange={(e) => setUserName(e.target.value)}
		            required
		        />
		    </Form.Group>

		    <Form.Group controlId="password" className="pb-4">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		            type="password" 
		            placeholder="Password"
		            value={password}
		            onChange={(e) => setPassword(e.target.value)}
		            required
		        />
		    </Form.Group>

		    { isActive ? 
		        <Button variant="primary" type="submit" id="submitBtn">
		            Submit
		        </Button>
		        : 
		        <Button variant="danger" type="submit" id="submitBtn" disabled>
		            Submit
		        </Button>
		    }

		</Form>
		</Container>
		       

  	);
}

