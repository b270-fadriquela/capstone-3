import Container from 'react-bootstrap/Container';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { Fragment, useState, useEffect, useContext} from 'react';
import { Link, NavLink} from 'react-router-dom';

import UserContext from '../context/UserContext';

import {Row, Col, Card, Button} from 'react-bootstrap';

export default function CategoryNavBar()
{
	const { user } = useContext(UserContext)

	return(
			<Navbar bg="light" expand="lg">
		      <Container fluid>


		          <Nav className="me-auto">
		          <Nav.Link as={Link} to="/">Home</Nav.Link>
		          <Nav.Link as={Link} to="/steam">Steam</Nav.Link>
		          <Nav.Link as={Link} to="/origin">Origin</Nav.Link>
		          
		          <Nav.Link as={Link} to="/ps5">PS5</Nav.Link>
		          <Nav.Link as={Link} to="/ps4">PS4</Nav.Link>

		         {(user.id !== null)
		          	?
		          	<Fragment>
		          <Nav.Link as={Link} to="/cart">Cart</Nav.Link>
		          <Nav.Link as={Link} to="/history">Order Status</Nav.Link>
		          </Fragment>
		          :
		          <Fragment/>
		      	}
		          </Nav>

		  		
		      </Container>
		    </Navbar>
	);
		
}

