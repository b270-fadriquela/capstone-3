import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';


//View
export default function UserItem({data}) {
	const {_id, userName, email } = data;

	const width = window.innerWidth;

	const promoteToUser = (userId) => {
		  fetch(`${process.env.REACT_APP_API_URL}/promote/${userId}`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		     
	      	 })
		    .then(res => res.json())
		    .then(data => {
		    	console.log("prmote " + data);
		    	Swal.fire({
		    		icon: "info",
		    		text: "User Promoted to Admin!"
		    	})
		    	window.location.reload();

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}



	return(

		<Container>
		<Row className='bg-success m-2 py-1'>
			<Col>
				{_id}
			</Col>

			<Col>
				{userName}
			</Col>

			<Col>
				{email}
			</Col>
			
			<Col>
			<Button variant="warning" onClick={() => promoteToUser(_id)}>PromoteUser</Button>		
			</Col>

		</Row>
		</Container>
		
	)

}

