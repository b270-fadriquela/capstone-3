


import {Row, Col, Card, Button, Form, Container, Dropdown} from 'react-bootstrap';

import { useState, useEffect, React, Fragment} from 'react';

import { Link, useParams } from 'react-router-dom';
import  Swal  from 'sweetalert2';

export default function CreateProduct() {

	const { productId } = useParams();
	const [changes, setChanges] = useState(false)

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [stocks, setStocks] = useState("")
	const [category, setCategory] = useState("")

	const createProduct = (e) => { 
		e.preventDefault();



	      	 fetch(`${process.env.REACT_APP_API_URL}/product`, {
		       	method: "POST",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		       	body: JSON.stringify({
		       		name: name,
		       		description: description,
		       		price: price,
		       		stocks: stocks,
		       		category: category,

		       	})
	      	 })


	        .then(res => res.json())
	        .then(data => {

	            if(data.errors === undefined)  
	            	Swal.fire({
		    		icon: "info",
		    		text: "Product Added!"
			    	})
	            setName("");
	            setDescription("");
	            setPrice("");
	            setStocks("");
	            setCategory("");


	            setTimeout(()=>{
	                  window.location.reload(false);
	              }, 2000);
	        })
	        .catch(rejected => {
	            console.log(rejected);
	        });


    }; 

    function uploadImage(e){
    	e.preventDefault();

    }

	return(
		<Container>
			<Card className="mt-2">
			<Container fluid>
					<h1> Creating new Product! </h1>
					<Button variant="danger" as={Link} to={`/Products/`}>Back</Button>
			</Container>
			<Form className="p-2" onSubmit={(e) => createProduct(e)}> 
					<h1>{}{productId}</h1>


					     <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Name</Form.Label>
					       <Form.Control 
					       		type="text" 
					       		placeholder="Enter Product Name..." 
					       		required
					       		onChange={e => setName(e.target.value)}
					       		value={name}/>
					     </Form.Group>

					     <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Description</Form.Label>
					       <Form.Control 
					       		type="text" 
					       		placeholder="Enter Product Description..." 
					       		required
					       		onChange={e => setDescription(e.target.value)}
					       		value={description}/>
					     </Form.Group>
					     <Row>
					       <Form.Label>Category</Form.Label>
					     <Col>
					      <Form.Group className="mb-3" controlId="category">
					       <Form.Control 
					       		disabled
					       		type="text" 
					       		placeholder="Select Category" 
					       		required
					       		onChange={e => setPrice(e.target.value)}
					       		value={category}/>
					     </Form.Group>
					     </Col>

					     <Col>
					     <Dropdown onSelect={function(e){setCategory(e)}}>
					          <Dropdown.Toggle variant="dark" id="dropdown-basic">
					          </Dropdown.Toggle>

					          <Dropdown.Menu>
					            <Dropdown.Item eventKey="steam">Steam</Dropdown.Item>
					            <Dropdown.Item eventKey="origin">Origin</Dropdown.Item>
					            <Dropdown.Item eventKey="ps5">PS5</Dropdown.Item>
					            <Dropdown.Item eventKey="ps4">PS4</Dropdown.Item>
					          </Dropdown.Menu>
				        </Dropdown>
					        </Col>
					        </Row>

					      <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Price</Form.Label>
					       <Form.Control 
					       		type="number" 
					       		placeholder="Enter Product Price..." 
					       		required
					       		onChange={e => setPrice(e.target.value)}
					       		value={price}/>
					     </Form.Group>

					      <Form.Group className="mb-3" controlId="productName">
					       <Form.Label>Product Stocks</Form.Label>
					       <Form.Control 
					       		type="number" 
					       		placeholder="Enter Product Stocks.." 
					       		required
					       		onChange={e => setStocks(e.target.value)}
					       		value={stocks}/>
					     </Form.Group>

					      <Form.Group className="mb-3" controlId="productName">
					       <Form.Label></Form.Label>
					       <Form.Control 
					       		type="file" 
					       		
					       		onChange={e => uploadImage(e)}/>
					       		
					     </Form.Group>


					    <Button variant="primary" type="submit">Finalize</Button>
					</Form>
			</Card>

			<Card className="cardCourses mt-2">
			</Card>

		</Container>
		
	);
}