import {Row, Col, Card, Button,Container} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';


import UserItem from '../../components/UserItem';
//View
export default function User() {

	const [user, setUser] = useState([]);

	useEffect(()=>{

	     fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
                   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
	         })
		    .then(res => res.json())
		    .then(data => {

		    	setUser (data.map(user =>{
		    		return(
		    			<UserItem key={user._id} data={user} />
		    		)
		    	}))	
		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [])


	return(
		<Container>
			 <h1>Active Users</h1>
			 {user}

		</Container>
		
	)

}