import {Row, Col, Card, Button, Form, Container, Dropdown} from 'react-bootstrap';

import { useState, useEffect, React, Fragment, useContext} from 'react';
import ReactSwitch from 'react-switch';
import Swal from 'sweetalert2';

import { Link } from 'react-router-dom';
import UserContext from '../context/UserContext';
import OrderItem from '../components/OrderItem';

export default function ViewCart() {

	const { user } = useContext(UserContext)

	const [cart, setCart] = useState([]);
	const [totalPrice, setTotalPrice] = useState([]);

	useEffect(()=>{

		const indexVal = 
		{
			index: 0
		};
	     fetch(`${process.env.REACT_APP_API_URL}/profile`, {
                   headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
	         })
		    .then(res => res.json())
		    .then(data => {

		    	let subtotal = 0;
			    	data.cart.forEach((data) => {
			    			console.log(subtotal);
			    		if(data.total !== undefined)
			    		subtotal += data.total

			    	})

			    	setTotalPrice(subtotal);

			    	setCart (data.cart.map(product =>{
			    		return(
			    			<OrderItem key={product.productId} data={product} index = {1} isCart={true}/>
			    		)
			    	}))	
		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [])

	console.log(cart.length)

	const Checkout = (e) => {
		  fetch(`${process.env.REACT_APP_API_URL}/checkout`, {
		       	method: "PATCH",
		       	headers: {
		       		"Content-Type" : "application/json",
		       		Authorization: `Bearer ${localStorage.getItem('token')}`
		       	},
		     
	      	 })
		    .then(res => res.json())
		    .then(data => {
		    	console.log("checkout ");
		    	 Swal.fire({
					title: "Success",
					icon: "info",
					text: "Checkout Complete"})

		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}

	return(
		<Container>
				<Row>
					<Col>
					
					{cart}
					</Col>


				</Row>
					<Row>
					<Col>
					Total Price:
					</Col>
					<Col>
					{totalPrice}
					</Col>


				</Row>
				<Row>

					{(cart.length === 0)
						?
						<Button variant="primary"  disabled={true}>Checkout</Button>
						:
						<Button variant="primary"  disabled={cart.length ===0} as={Link} to={`/history`}  onClick={(e) => Checkout(e)}>Checkout</Button>
					}
			    	
				
		    	
				</Row>

		</Container>
		
	);
}