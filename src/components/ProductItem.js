import {Row, Col, Card, Button} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';


//View
export default function ProductItem({data}) {
	const {_id, name, description, price, stocks, createdOn , isActive} = data;

	const width = window.innerWidth;

	const rowStyle = () => {

		if(isActive === true){
			return 'bg-success m-2 py-1';
		}
		else if(isActive === undefined){
			return 'bg-dark m-2 py-3';
		}
		else{
			return 'bg-danger m-2  py-1'
		}


	} 

	console.log("data: " + data);

	return(

		<Fragment>
		<Row className={rowStyle()}>
			<Col>
				{_id}
			</Col>

			<Col>
				{name}
			</Col>

			<Col>
				{description}
			</Col>

			<Col>
				{price}
			</Col>

			<Col>
				{stocks}
			</Col>

			<Col>
				{createdOn}
			</Col>

			{(isActive !== undefined)
			?
			<Col >
				<Button className="me-2" variant="primary" as={Link} to={`/Products/${_id}`}>View</Button>			
				<Button className="me-auto" variant="warning" as={Link} to={`/Products/${_id}/delete`}>Delete</Button>
			</Col>
			:
			<Col>
			</Col>
			}

		</Row>
		</Fragment>
		
	)

}

